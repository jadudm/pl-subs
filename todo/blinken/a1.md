---
---

I write this website as a resource for you throughout the term. It always reflects my best attempt to present material and assignments clearly and just-in-time with respect to your learning. While I will provide you with concise summaries of assignments and/or how to submit work, **I expect that you read what I write**, and that you do so carefully. This is not, I do not believe, an outrageous or even unreasonable assumption on my part.

## TL;DR

For this assignment, you should read/watch two things:

* [Electronic Innovations](#innovations)
* [Syllabus]({{site.data.urls.syllabus}})

There are prompted questions to answer in response to this work, which is described in the [Submission](#submission) section of this page.

## Electronic Innovations
<a name="innovations"> </a>

You probably have some ideas about what the study of electricity and electronics *is*. For example, you may have helped do some wiring in a house before. You may have had a limited exposure to electronics on the lab bench through a course in high school, or you may have had a family member (parent, aunt or uncle, cousin, what-have-you) who was particularly technically minded. As a result, **you probably have some preconceived notions of what it means to study and build electronic devices**.

Watch the two TED talks below; each is around 5 minutes long. Then, respond to the two short prompts that follow.

* [Leah Buechley: How to "sketch" with electronics](http://www.ted.com/talks/leah_buechley_how_to_sketch_with_electronics)

* [Ayah Bdeir: Building blocks that blink, beep and teach](http://www.ted.com/talks/ayah_bdeir_building_blocks_that_blink_beep_and_teach#t-231012)

## Why Standards-Based Assessment Matters

The second part of your assignment is to [review the syllabus]({{site.data.urls.syllabus}}). You need to review the syllabus this term because it embodies some ideas (standards-based assessment, in particular) that might be new. However, they represent my best attempt to respond to feedback I have received from Berea students over the past several terms. 

1. **Students have asked for transparency**. 

    I have had students ask, on more than one occasion, for more information about "how they are doing" in a class. Standards-based assessment makes this extremely clear: you and I work together on completing standards, and when you complete them, they get "checked off." As a result, you know exactly how you are doing.

1. **Grades are not for differentiating students**. 

    My job is not to separate you into bins. Instead, my job is to support you in your growth as computer scientists, and to prepare you to learn, life-long, in this field (or, really, any field). Therefore, if I can make my learning objectives crystal clear, then we can collaborate on your learning and development, as opposed to any artificial tension between us surrounding assessment.
    
1. **There is no normal curve in education**.

    The assumption that grades should be "evenly distributed" is gross. A normal curve suggests a random variable; excellent education should be anything but. If I am doing my job well, and you are working hard, it should be possible for all of you to be **excellent**. Anything else, and one of us is not doing our jobs.
    
1. **Your performance is not determined in relation to others**. 

    There is no universe where if you do well, and someone does better, then you should be ranked as being a less-capable learner. This is what "grading to a curve" suggests. Instead, all students should be held to high standards, and encouraged to excel in the pursuit of those standards. 
    
1. **Low grades do not make you work harder**.

    If I issue challenging exams early in the term, expecting you to do poorly, it will not "motivate you." It will discourage you. Instead, if you are working hard, then it should be possible to demonstrate your learning as you master material on your schedule, not mine. That said, we are constrained by the timeframe of a semester. For that reason, there will be deadlines and expectations that you keep those deadlines. However, there will be ample opportunity for revision, as the goal is not to "get an A" at the time that I say you should, but instead to demonstrate the achievement of a standard.
    
These ideas are not my own; they are supported by educational research regarding effective assessment. However, they are not "how things have always been done," and therefore, they might appear new to you. Many of the bullets presented above are [inspired by the work of Thomas Guskey]({{site.data.urls.guskey}}), a professor of education at the University of Kentucky. For those who are interested, I will gladly provide additional references.

## Submission

You have four prompts that you need to complete in response to the videos watched and the syllabus.

<script type="text/javascript" src="http://form.jotformpro.com/jsform/50043752371953"></script>
